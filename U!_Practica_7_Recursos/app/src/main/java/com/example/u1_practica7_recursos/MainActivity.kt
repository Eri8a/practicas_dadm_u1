package com.example.u1_practica7_recursos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnRes.setOnClickListener {
           ivimage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.imagen2))
        }
        btnURL.setOnClickListener {
          Picasso.get().load("https://www.cmtv.com.ar/imagenes_artistas/1691.jpg?Juan%20Gabriel")
              .into(ivimage)
        }
    }
}