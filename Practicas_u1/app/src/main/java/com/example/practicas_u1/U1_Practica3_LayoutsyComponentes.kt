package com.example.practicas_u1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_u1__practica3__layoutsy_componentes.*

class U1_Practica3_LayoutsyComponentes : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_u1__practica3__layoutsy_componentes)
        

        btnShow.setOnClickListener{
            if (etName.text.isNotEmpty() && etLastName.text.isNotEmpty() && etAge.text.isNotEmpty()){
              val persona =  when {
                    rbMen.isChecked -> {
                         Persona(etName.text.toString(), etLastName.text.toString(), etAge.text.toString().toInt(),  "Hombre")
                    }
                    rbWoman.isChecked -> {
                      Persona(etName.text.toString(), etLastName.text.toString(), etAge.text.toString().toInt(),  "Mujer")
                    }
                    else -> {
                          Persona(etName.text.toString(), etLastName.text.toString(), etAge.text.toString().toInt(),  "No especifica")
                    }
                }
                   Toast.makeText(this , "Persona de nombre: ${persona.name} ${persona.LastName}\n" +
                           "Edad: ${persona.age}\nDe genero: ${persona.gender}", Toast.LENGTH_LONG).show()
    }
                   Toast.makeText(this, "Por favor rellena la Informacion solicitada", Toast.LENGTH_LONG)
}

        }
}
data class Persona (var name:  String, var LastName: String, var age: Int, var gender: String)

