package com.example.practicas_u1

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import android.widget.RadioButton
import android.widget.Toast
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_u1__practica3__layoutsy_componentes.*
import kotlinx.android.synthetic.main.activity_u1__practica4__listeners__bottons__radiogroup.*

class U1_Practica4_Listeners_Bottons_Radiogroup : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_u1__practica4__listeners__bottons__radiogroup)

        btnListener.setOnClickListener{
            Toast.makeText(this, "Boton por Listener", Toast.LENGTH_LONG).show()
        }

        btnClase.setOnClickListener(this)

        /*if (radioButton.isChecked){
        Toast.makeText(this. ""Esta marcado", Toast.LENGTH_SHORT).SHOW()
        }*/

        rgGroup.setOnCheckedChangeListener {group, checkid ->
            when (group.findViewById<RadioButton>(checkid)){
                rbBlack -> {
                    Toast.makeText(this, "Esta Marcado Negro", Toast.LENGTH_SHORT).show()
                    root.setBackgroundColor(Color.YELLOW)
                }
                rbWhite ->{
                    Toast.makeText(this, "Esta Marcado Blanco", Toast.LENGTH_SHORT).show()
                    root.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
                }
                rbOthers ->{
                    Toast.makeText(this, "Esta Marcado Otro", Toast.LENGTH_SHORT).show()
                    root.setBackgroundColor(ContextCompat.getColor(this, R.color.mio))
                }
            }
        }
    }

    fun onXmlClick(view: View){
        Toast.makeText(this, "Boton por Listener", Toast.LENGTH_LONG).show()
        if (rbBlack.isChecked){
            rbBlack.setTextColor(ContextCompat.getColor(this, R.color.white))
            rbWhite.setTextColor(ContextCompat.getColor(this, R.color.white))
            rbOthers.setTextColor(ContextCompat.getColor(this, R.color.white))
    }
        }
    override fun onClick(v: View?) {
        Toast.makeText(this, "Boton por Clase", Toast.LENGTH_LONG).show()
    }

    /*override fun onClick(v: View?) {-------Otra forma de hacer Click en botones-------------------
    val button = v as Button
    when(button.id){
       R.id.btnlistener -> {
       Toast.makeText(this, "BtnListener", Toast.LENGTH_LONG).show()
        }
       R.id.btnClase -> {
       Toast.makeText(this, "BtnClase", Toast.LENGTH_LONG).show()
         }
       R.id.btnXml ->{
       Toast.makeText(this, "BtnXml", Toast.LENGTH_LONG).show()
        } ------------------------------------------------------------------------------------------
     */
}