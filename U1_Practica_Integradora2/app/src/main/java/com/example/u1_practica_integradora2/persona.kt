package com.example.u1_practica_integradora2

import android.os.Parcel
import android.os.Parcelable

data class persona(var nombre: String?, var apellido: String?, var edad: Int, var organizacion: String?, var correo: String?, var sexo: String?) :
    Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(nombre)
        parcel.writeString(apellido)
        parcel.writeInt(edad)
        parcel.writeString(organizacion)
        parcel.writeString(correo)
        parcel.writeString(sexo)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<persona> {
        override fun createFromParcel(parcel: Parcel): persona {
            return persona(parcel)
        }

        override fun newArray(size: Int): Array<persona?> {
            return arrayOfNulls(size)
        }
    }


}
