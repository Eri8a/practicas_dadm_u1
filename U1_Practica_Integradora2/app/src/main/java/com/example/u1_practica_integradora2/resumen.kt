package com.example.u1_practica_integradora2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.u1_practica_integradora2.R.drawable
import com.tec.u1_practica_integradora_2.CustomAdapter

class resumen : AppCompatActivity() {

    val userList = mutableListOf<persona>()

    private val adapter by lazy{
        CustomAdapter { persona, pos ->
            Toast.makeText(this, "Persona $pos: ${persona.nombre}, ${persona.edad}", Toast.LENGTH_SHORT).show()
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resumen)
        val list = intent.extras?.getSerializable("persona") as MutableList<persona>
        userList.addAll(list)
        var rvPersona: RecyclerView = findViewById(R.id.rvPersonas)
        rvPersona.adapter = adapter
        adapter.setList(userList)
        //var arr = this.getIntent().getParcelableArrayListExtra<Parcelable>("persona")
        /*var extras = intent.extras
        var arr = extras?.getParcelableArrayList<persona>("persona")
        var nombre: TextView = findViewById(R.id.txtNombre)
        var apellido: TextView = findViewById(R.id.txtApellidos)
        var edad: TextView = findViewById(R.id.txtEdad)
        var imagen: ImageView = findViewById(R.id.imageSexo)
        var org: TextView = findViewById(R.id.txtOrg)
        var correo: TextView = findViewById(R.id.txtCorreo)

        //nombre.setText(arr!![0]?.toString())
        nombre.setText(arr?.get(0)?.nombre.toString())
        apellido.setText(arr?.get(0)?.apellido.toString())
        edad.setText("("+arr?.get(0)?.edad.toString()+"años)")
        org.setText(arr?.get(0)?.organizacion.toString())
        correo.setText(arr?.get(0)?.correo.toString())
        var sexo: String = arr?.get(0)?.sexo.toString()
        if(sexo.equals("Hombre")){
            imagen.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.icono_hombre_dos))
        }else{
            imagen.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.icono_mujer))
        }*/

    }
}


